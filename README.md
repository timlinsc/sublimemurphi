Sublime Text Murphi Syntax Support Package
==========================================


Description
-----------

The Murphi description language accompanies the Murphi Verifcation System, a tool developed at Stanford for protocol verifcation. It is Pascal-like in format. The purpose of this package is to provide basic syntax highlighting for the language. This syntax package has the following features:

1. Highlights reserved words, comments, constants, and predefined functions
2. Recognizes function and procedure definitions 

Setup
-----

To get Sublime to recognize Murphi files, open a Murphi description language file (.m) in Sublime and select "Murphi" under View -> Syntax -> Open all with current extension as...